package com.loser.gotravel.secimpl;

import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.security.crypto.codec.Base64;

import com.loser.gotravel.dao.AbstractDao;
import com.loser.gotravel.models.User;
import com.loser.gotravel.respsec.TokenInfo;
import com.loser.gotravel.respsec.TokenManager;

public class TokenManagerSingle extends AbstractDao implements TokenManager {
	
	public TokenManagerSingle(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	private TokenInfo tokenInfo = new TokenInfo();

	@Override
	public TokenInfo createNewToken(long userId, String deviceId, String loginType) {
		String token;
		do {
			token = generateToken();
			tokenInfo = (TokenInfo) getSession().createCriteria(TokenInfo.class).add(Restrictions.eq("token", token))
					.uniqueResult();
		} while (tokenInfo != null);
		tokenInfo = new TokenInfo(token, userId, deviceId, loginType);
		TokenInfo existToken = (TokenInfo) getSession().createCriteria(TokenInfo.class)
				.add(Restrictions.eq("userId", userId))
				.add(Restrictions.eq("deviceId", deviceId)).uniqueResult();
		if (existToken != null) {
			existToken.token = tokenInfo.token;
			existToken.timeOut = new Date(System.currentTimeMillis());
			getSession().update(existToken);
		} else {
			getSession().save(tokenInfo);
		}
		return tokenInfo;
	}

	private String generateToken() {
		byte[] tokenBytes = new byte[32];
		new SecureRandom().nextBytes(tokenBytes);
		return new String(Base64.encode(tokenBytes), StandardCharsets.UTF_8);
	}

	@Override
	public void removeAllUserById(long userId) {
		Query query = sessionFactory.getCurrentSession()
				.createQuery("delete TokenInfo where userId = " + String.valueOf(userId));
		query.executeUpdate();
	}

	@Override
	public void removeToken(String token) {
		Query query = sessionFactory.getCurrentSession().createQuery("delete TokenInfo where token = " + token);
		query.executeUpdate();
	}

	@Override
	public User getUser(String token) {
		TokenInfo tokenInfo = (TokenInfo) getSession().createCriteria(TokenInfo.class)
				.add(Restrictions.eq("token", token)).uniqueResult();
		User user = (User) getSession().createCriteria(User.class).add(Restrictions.eq("userId", tokenInfo.userId))
				.uniqueResult();
		return user;
	}

	@Override
	public Collection<TokenInfo> getUserTokens(long userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Long> getValidUsers() {
		// TODO Auto-generated method stub
		return null;
	}

}
