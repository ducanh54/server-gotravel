package com.loser.gotravel.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Like {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long likeId;
	
	public Long userId;
	@Column(nullable = false)
	public Long relationLocationId; // 1 = hotel, 2 = restaurants, 3 = shopping, 4 = entertaiment;
	public Integer like; // 1 = like, 0 = unlike;
	
	public Date createdTime;
	public Date modifiedTime;
	
}
