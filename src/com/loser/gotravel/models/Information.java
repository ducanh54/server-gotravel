package com.loser.gotravel.models;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Information {
	public String title;
	@Column(columnDefinition = "text")
	public String description;
}
