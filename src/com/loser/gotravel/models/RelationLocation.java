package com.loser.gotravel.models;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
public class RelationLocation {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long relationLocationId;
	
	public Long attractionsId;
	public String type; // 1: Hotel , 2: Restaurent, 3: Shop, 4: Entertainment;
	@Column(nullable = false)
	public String name;
	public String price;
	@Column(columnDefinition = "text")
	public String description;
	public String status;
	public String photoMapUrl;
	
	public Double lat;
	public Double lng;
	public Integer totalLike;
	public Integer totalUnLike;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@ElementCollection(targetClass = Photo.class)
	public Collection<Photo> photos;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@ElementCollection(targetClass = Contact.class)
	public Collection<Contact> contacts;
	
	public Date createdTime;
	public Date modifiedTime;
}
