package com.loser.gotravel.models;

import javax.persistence.Embeddable;

@Embeddable
public class Photo {
	public String photoUrl;
	public String description;
}
