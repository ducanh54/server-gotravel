package com.loser.gotravel.models;

public enum TypeLocation {
	
	HOTEL("hotel"), RESTAURENT("restaurent"), SHOP("shop"), ENTERTAINMENT("entertainment");
	
	private String typeLocation;
	
	private TypeLocation(String typeLocation) {
		this.typeLocation = typeLocation;
	}
	
	@Override
	public String toString() {
		return this.typeLocation;
	}
}
