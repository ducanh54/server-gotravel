package com.loser.gotravel.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "user")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long userId;
	@Column(name = "name", length = 40)
	public String name;
	@Column(name = "username", unique = true, nullable = false, length = 30)
	public String username;
	@Column(name = "email", unique = true, nullable = false, length = 30)
	public String email;
	public String avatar;
	public String facebookId;
	public String gmailId;
	public String password;
	public String phone;
	
	@Column(name = "createdTime", nullable = false)
	@JsonIgnore
	public Date createdTime;
	
	@Column(name = "modifiedTime", nullable = false)
	@JsonIgnore
	public Date modifiedTime;
	
}
