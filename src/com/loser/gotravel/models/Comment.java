package com.loser.gotravel.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class Comment {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long commentId;
	public Long userId;
	public Long attractionsId;
	@Column(columnDefinition = "text")
	public String content;
	
	@Transient
	public String avatar;
	
	@Transient
	public String username;
	
	public Date createdTime;
	public Date modifiedTime;
	
}
