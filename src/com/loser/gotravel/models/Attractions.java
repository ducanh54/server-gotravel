package com.loser.gotravel.models;

import java.util.Collection;
import java.util.Date;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Attractions {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long attractionsId;
	public Long categoryId;
	public String name;
	public String description;
	public String address;
	public Double varRate;
	public String classify;
	@Transient
	public Integer totalRate;
	
	@Transient
	public Double yourRate;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@ElementCollection(targetClass = Information.class)
	public Collection<Information> informations;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@ElementCollection(targetClass = Photo.class)
	public Collection<Photo> photos;
	
	@JsonIgnore
	public Date createdTime;
	@JsonIgnore
	public Date modifiedTime;
	
}
