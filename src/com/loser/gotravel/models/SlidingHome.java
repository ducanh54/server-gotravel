package com.loser.gotravel.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class SlidingHome {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long slidingHomeId;
	
	public String name;
	public String description;
	public String imageUrl;
	
	public Date createdTime;
	public Date modifiedTime;
	
	public SlidingHome() {
		super();
	}

	public SlidingHome(String name, String description, String imageUrl) {
		super();
		this.name = name;
		this.description = description;
		this.imageUrl = imageUrl;
	}
	
	
	
}
