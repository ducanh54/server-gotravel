package com.loser.gotravel.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Category {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long categoryId;
	public String name;
	public String description;
	public String image;
	
	public Date createdTime;
	public Date modifiedTime;
	
	public Category() {
		super();
	}

	public Category(String name, String description,
			String image) {
		super();
		this.name = name;
		this.description = description;
		this.image = image;
	}
	
	
}
