package com.loser.gotravel.models;

import javax.persistence.Embeddable;

@Embeddable
public class Contact {
	
	public String contactName;
	public String contactPhone;
	public String contactEmail;
	public String contactAddress;
	public String contactFax;
	
}
