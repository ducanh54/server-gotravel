package com.loser.gotravel.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Rate {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long rateId;
	public Long userId;
	public Long attractionsId;
	public Double rate;
	
	public Date createdTime;
	public Date modifiedTime;
}
