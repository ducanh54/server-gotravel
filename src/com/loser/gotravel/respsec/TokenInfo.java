package com.loser.gotravel.respsec;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "token_info")
public class TokenInfo {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long tokenId;
	@Column(nullable = false)
	public String token;
	@Column(nullable = false)
	public long userId;
	@Column(nullable = false)
	public Date timeOut;
	@Column(name = "deviceId")
	public String deviceId;
	@Column(name = "loginType", nullable = false)
	public String loginType;

	public TokenInfo() {
	}

	public TokenInfo(String token, long userId, String deviceId, String loginType) {
		this.token = token;
		this.userId = userId;
		this.deviceId = deviceId;
		this.loginType = loginType;
		timeOut = new Date(System.currentTimeMillis());
	}

	public String getToken() {
		return token;
	}
}
