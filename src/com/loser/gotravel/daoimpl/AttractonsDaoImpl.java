package com.loser.gotravel.daoimpl;

import org.hibernate.SessionFactory;

import com.loser.gotravel.dao.AbstractDao;
import com.loser.gotravel.dao.AttractionsDao;
import com.loser.gotravel.models.Attractions;

public class AttractonsDaoImpl extends AbstractDao implements AttractionsDao {

	public AttractonsDaoImpl(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	@Override
	public Long saveItem(Attractions touristResort) {
		Long autoId = (Long) getSession().save(touristResort);
		return autoId;
	}

	@Override
	public Attractions getActtractionsById(Long attactionsId) {
		Attractions attractions = (Attractions) getSession().get(Attractions.class, attactionsId);
		return attractions;
	}

	@Override
	public boolean deleteAttractionsById(Long attactionsId) {
		Attractions attractions = (Attractions) getSession().get(Attractions.class, attactionsId);
		if (attractions != null) {
			getSession().delete(attactionsId);
			return true;
		}
		return false;
	}

	@Override
	public void updateAttractions(Attractions attractions) {
		getSession().update(attractions);
	}

}
