package com.loser.gotravel.daoimpl;

import org.hibernate.SessionFactory;

import com.loser.gotravel.dao.AbstractDao;
import com.loser.gotravel.dao.CommentDao;
import com.loser.gotravel.models.Comment;

public class CommentDaoImpl extends AbstractDao implements CommentDao {

	public CommentDaoImpl(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	@Override
	public Long saveComment(Comment comment) {
		Long autoId = (Long) getSession().save(comment);
		return autoId;
	}

	@Override
	public void updateComment(Comment comment) {
		getSession().update(comment);
	}

	@Override
	public boolean deleteComment(Long commentId) {
		Comment comment = (Comment) getSession().get(Comment.class, commentId);
		if (comment != null) {
			getSession().delete(comment);
			return true;
		}
		return false;
	}
	
}
