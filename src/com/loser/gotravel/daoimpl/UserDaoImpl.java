package com.loser.gotravel.daoimpl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import com.loser.gotravel.dao.AbstractDao;
import com.loser.gotravel.dao.UserDao;
import com.loser.gotravel.models.User;

public class UserDaoImpl extends AbstractDao implements UserDao {

	public UserDaoImpl(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	@Override
	public Long saveUser(User user) {
		Long autoId = (Long) getSession().save(user);
		return autoId;
	}

	@Override
	public User getUserById(Long userId) {
		User user = (User) getSession().get(User.class, userId);
		return user;
	}

	@Override
	public User getUSerByUsername(String username) {
		User user = (User) getSession().createCriteria(User.class).add(Restrictions.eq("username", username)).uniqueResult();
		return user;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> getListAllUser() {
		List<User> listUsers = (List<User>) getSession().createCriteria(User.class).list();
		return listUsers;
	}

	@Override
	public void updateUser(User user) {
		getSession().update(user);
	}

	@Override
	public boolean deleteUserById(Long userId) {
		User user = (User) getSession().get(User.class, userId);
		if (user != null) {
			getSession().delete(user);
			return true;
		} else {
			return false;
		}
		
	}

	@Override
	public boolean deleleUserByUsername(String username) {
		User user = (User) getSession().createCriteria(User.class).add(Restrictions.eq("username", username)).uniqueResult();
		if (user != null) {
			getSession().delete(user);
			return true;
		} else {
			return false;
		}
	}

}
