package com.loser.gotravel.utils;

public class Message {
	/** Return message success */
	public final static String SUCCESS  = "Successfully";
	
	/** Return message error */
	public final static String INVALID_TOKEN = "Invalid token";
	public final static String CAN_NOT_INSERT_DB = "Can not insert database, please again.";
	
	/* API User Controller */
	public final static String USER_NOT_FOUND = "User not found";
	public final static String TOKEN_IS_NULL = "Token is null";
	public final static String LOGIN_SUCCESS = "Login successfully";
	public final static String REGISTER_SUCCESS = "Register successfully";
	/* API Category Controller */
	public final static String CATEGORY_NOT_FOUND = "Category not found";
	public final static String LIST_CATEGORY_MAXIMUM = "Category have maximum";
	/* API SlidingHome Controller */
	public final static String SLIDING_HOME_MAXIMUM = "Sliding home maximum";
	public final static String SLIDING_NOT_FOUND = "Sliding home not found";
	/* API Attractions Controller */
	public final static String ATTRACTIONS_CATEGORYID_NOT_NULL = "Category Id must be not null or empty";
	public final static String ATTRACTIONS_NAME_NOT_NULL = "Name must be not null or empty";
	public final static String ATTRACTIONS_CLASSIFY_NOT_NULL = "Classify must be not null or empty";
	public final static String ATTRACTIONS_ID_NOT_NULL = "Attractions Id must be not null or empty for update";
	public final static String ATTRACTIONS_INFORMATION_NOT_NULL = "Information must be not null or empty";
	public final static String ATTRACTIONS_CAN_NOT_FOUND = "Attractions can not found";
	public final static String ATTRACTIONS_RATE_FAIL = "Rate point must be from 1 to 5";
	public final static String ATTRACTIONS_COMMENT_LENGTH = "Content must be not less than 10 characters";
	/* API Relation Location Controller */
	public final static String RELATION_LOCATION_NAME_NOT_NULL = "Name must be not null is empty";
	public final static String RELATION_LOCATION_CONTACT_NOT_NULL = "Contact must be not null is empty";
	public final static String RELATION_LOCATION_TYPE_NOT_NULL = "Type must be not null is empty";
	public final static String RELATION_LOCATION_TYPE_WRONG = "Type is wrong";
	public final static String RELATION_LOCATION_USERLIKE = "Like is wrong";
	public final static String RELATION_LOCATION_USERLIKED = "You are liked this";
	
	
}
