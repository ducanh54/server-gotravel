package com.loser.gotravel.dao;

import com.loser.gotravel.models.Comment;

public interface CommentDao {
	
	Long saveComment(Comment comment);

	void updateComment(Comment comment);
	
	boolean deleteComment(Long commentId);
}
