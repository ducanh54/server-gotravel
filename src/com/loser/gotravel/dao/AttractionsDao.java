package com.loser.gotravel.dao;

import com.loser.gotravel.models.Attractions;

public interface AttractionsDao {
	
	Long saveItem(Attractions touristResort);
	
	Attractions getActtractionsById(Long attactionsId);
	
	void updateAttractions(Attractions attractions);
	
	boolean deleteAttractionsById(Long attactionsId);
	
}
