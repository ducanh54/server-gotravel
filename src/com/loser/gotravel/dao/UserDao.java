package com.loser.gotravel.dao;

import java.util.List;

import com.loser.gotravel.models.User;

public interface UserDao {
	
	Long saveUser(User user);
	
	User getUserById(Long userId);
	
	User getUSerByUsername(String username);
	
	List<User> getListAllUser();
	
	void updateUser(User user);
	
	boolean deleteUserById(Long userId);
	
	boolean deleleUserByUsername(String username);
	
	
}
