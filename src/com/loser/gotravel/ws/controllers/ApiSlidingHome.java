package com.loser.gotravel.ws.controllers;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.loser.gotravel.models.SlidingHome;
import com.loser.gotravel.utils.CommonUtils;
import com.loser.gotravel.utils.Message;
import com.loser.gotravel.ws.response.ResponseListObjects;
import com.loser.gotravel.ws.response.ResponseObjectDetail;

@RestController
@RequestMapping("/api/sliding-home")
@Transactional
public class ApiSlidingHome extends BaseController {
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public @ResponseBody ResponseObjectDetail<Object> create(HttpSession httpSession,
			@RequestParam(value = "description", required = false, defaultValue ="") String description,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "imageUrl", required = true) String imageUrl) {
		@SuppressWarnings("unchecked")
		List<SlidingHome> listSlidingHomes = getSession().createCriteria(SlidingHome.class).list();
		if (listSlidingHomes.size() >= 6) {
			return new ResponseObjectDetail<Object>(false, Message.SLIDING_HOME_MAXIMUM, null);
		}
		SlidingHome slidingHome = new SlidingHome(name, description, imageUrl);
		slidingHome.createdTime = slidingHome.modifiedTime = new Date(System.currentTimeMillis());
		Long autoId = (Long) getSession().save(slidingHome);
		if (autoId > 0) {
			return new ResponseObjectDetail<Object>(true, Message.SUCCESS, new Long(autoId));
		}
		return new ResponseObjectDetail<Object>(false, Message.CAN_NOT_INSERT_DB, new Long(autoId));
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody ResponseObjectDetail<Object> update(HttpSession httpSession,
			@RequestParam(value = "slidingHomeId", required = true) Long slidingHomeId,
			@RequestParam(value = "description", required = false) String description,
			@RequestParam(value = "name", required = false) String name,
			@RequestParam(value = "imageUrl", required = false) String imageUrl) {
		SlidingHome slidingHome = (SlidingHome) getSession().get(SlidingHome.class, slidingHomeId);
		if (slidingHome != null) {
			if (CommonUtils.checkStringValid(name)) slidingHome.name = name;
			if (CommonUtils.checkStringValid(imageUrl)) slidingHome.imageUrl = imageUrl;
			if (CommonUtils.checkStringValid(description)) slidingHome.description = description;
			slidingHome.modifiedTime = new Date(System.currentTimeMillis());
			getSession().update(slidingHome);
			return new ResponseObjectDetail<Object>(true, Message.SUCCESS, null);
		}
		return new ResponseObjectDetail<Object>(false, Message.SLIDING_NOT_FOUND, null);
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public @ResponseBody ResponseListObjects<Object> list(HttpSession httpSession) {
		@SuppressWarnings("unchecked")
		List<SlidingHome> listSlidingHomes = getSession().createCriteria(SlidingHome.class).list();
		return new ResponseListObjects<Object>(true, Message.SUCCESS, (Integer) listSlidingHomes.size(), listSlidingHomes);
	}
	
}
