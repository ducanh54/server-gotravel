package com.loser.gotravel.ws.controllers;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.loser.gotravel.dao.UserDao;
import com.loser.gotravel.models.User;
import com.loser.gotravel.respsec.TokenInfo;
import com.loser.gotravel.respsec.TokenManager;
import com.loser.gotravel.utils.Message;
import com.loser.gotravel.ws.response.ResponseDataLogin;
import com.loser.gotravel.ws.response.ResponseListObjects;
import com.loser.gotravel.ws.response.ResponseObjectDetail;

@RestController
@Transactional
@RequestMapping("/api/user")
public class ApiUserController extends BaseController {
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	TokenManager tokenManager;
	
	private static final int LOGIN_FACEBOOK = 1;
	private static final int LOGIN_GMAIL = 2;
	
	@RequestMapping(value = "/demo", method = RequestMethod.GET)
	public @ResponseBody ResponseObjectDetail<Object> hello() {
		return new ResponseObjectDetail<Object>(true, "Successfully", null);
	}
	
	@RequestMapping(value = "/login-with-facebook", method = RequestMethod.POST)
	public @ResponseBody ResponseObjectDetail<Object> loginWithFacebook(HttpSession httpSession,
			@RequestParam(value = "name", required = false, defaultValue = "") String name,
			@RequestParam(value = "username", required = true) String username,
			@RequestParam(value = "email", required = true) String email,
			@RequestParam(value = "avatar", required = false, defaultValue = "") String avatar,
			@RequestParam(value = "facebookId", required = true) String facebookId,
			@RequestParam(value = "phone", required = false, defaultValue = "") String phone,
			@RequestParam(value = "deviceId", required = true) String deviceId) {
		User userExist = userDao.getUSerByUsername(username);
		if (userExist == null) {
			User user = new User();
			user.name = name;
			user.username = username;
			user.avatar = avatar;
			user.email = email;
			user.phone = phone;
			user.facebookId = facebookId;
			user.createdTime = user.modifiedTime = new Date(System.currentTimeMillis());
			Long autoId = userDao.saveUser(user);
			
			if (autoId > 0) {
				TokenInfo tokenInfo = tokenManager.createNewToken(autoId, deviceId, String.valueOf(LOGIN_FACEBOOK));
				if (tokenInfo == null) {
					return new ResponseObjectDetail<Object>(true, Message.TOKEN_IS_NULL, null);
				}
				ResponseDataLogin dataLogin = new ResponseDataLogin(tokenInfo.getToken(), user);
				return new ResponseObjectDetail<Object>(true, Message.REGISTER_SUCCESS, dataLogin);
			} else {
				return new ResponseObjectDetail<Object>(true, Message.CAN_NOT_INSERT_DB, null);
			}
		} else {
			TokenInfo tokenInfo = tokenManager.createNewToken(userExist.userId, deviceId, String.valueOf(LOGIN_FACEBOOK));
			if (tokenInfo == null) {
				return new ResponseObjectDetail<Object>(true, Message.TOKEN_IS_NULL, null);
			}
			userExist.facebookId = facebookId;
			userDao.updateUser(userExist);
			ResponseDataLogin dataLogin = new ResponseDataLogin(tokenInfo.getToken(), userExist);
			return new ResponseObjectDetail<Object>(true, Message.LOGIN_SUCCESS, dataLogin);
		}
	}
	
	@RequestMapping(value = "/login-with-gmail", method = RequestMethod.POST)
	public @ResponseBody ResponseObjectDetail<Object> loginWithGmail(HttpSession httpSession,
			@RequestParam(value = "name", required = false, defaultValue = "") String name,
			@RequestParam(value = "username", required = true) String username,
			@RequestParam(value = "email", required = true) String email,
			@RequestParam(value = "avatar", required = false, defaultValue = "") String avatar,
			@RequestParam(value = "gmailId", required = true) String gmailId,
			@RequestParam(value = "phone", required = false, defaultValue = "") String phone,
			@RequestParam(value = "deviceId", required = true) String deviceId) {
		User userExist = userDao.getUSerByUsername(username);
		if (userExist == null) {
			User user = new User();
			user.name = name;
			user.username = username;
			user.avatar = avatar;
			user.email = email;
			user.phone = phone;
			user.gmailId = gmailId;
			user.createdTime = user.modifiedTime = new Date(System.currentTimeMillis());
			Long autoId = userDao.saveUser(user);
			
			if (autoId > 0) {
				TokenInfo tokenInfo = tokenManager.createNewToken(autoId, deviceId, String.valueOf(LOGIN_GMAIL));
				if (tokenInfo == null) {
					return new ResponseObjectDetail<Object>(true, Message.TOKEN_IS_NULL, null);
				}
				ResponseDataLogin dataLogin = new ResponseDataLogin(tokenInfo.getToken(), user);
				return new ResponseObjectDetail<Object>(true, Message.REGISTER_SUCCESS, dataLogin);
			} else {
				return new ResponseObjectDetail<Object>(true, Message.CAN_NOT_INSERT_DB, null);
			}
		} else {
			TokenInfo tokenInfo = tokenManager.createNewToken(userExist.userId, deviceId, String.valueOf(LOGIN_GMAIL));
			if (tokenInfo == null) {
				return new ResponseObjectDetail<Object>(true, Message.TOKEN_IS_NULL, null);
			}
			userExist.gmailId = gmailId;
			userDao.updateUser(userExist);
			ResponseDataLogin dataLogin = new ResponseDataLogin(tokenInfo.getToken(), userExist);
			return new ResponseObjectDetail<Object>(true, Message.LOGIN_SUCCESS, dataLogin);
		}
	}
	
	
	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public @ResponseBody ResponseObjectDetail<Object> profile(HttpSession httpSession) {
		if (checkToken(httpSession)) {
			User user = (User) userDao.getUserById(token.userId);
			if (user != null) {
				return new ResponseObjectDetail<Object>(true, Message.SUCCESS, user);
			} 
			return new ResponseObjectDetail<Object>(false, Message.USER_NOT_FOUND, null);
		}
		return new ResponseObjectDetail<Object>(false, Message.INVALID_TOKEN, null);
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public @ResponseBody ResponseListObjects<Object> listUsers(HttpSession httpSession) {
		List<User> listUsers = userDao.getListAllUser();
		return new ResponseListObjects<Object>(true, Message.SUCCESS, (Integer) listUsers.size(), listUsers);
	}
}
