package com.loser.gotravel.ws.controllers;

import java.util.Date;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.loser.gotravel.models.Category;
import com.loser.gotravel.utils.CommonUtils;
import com.loser.gotravel.utils.Message;
import com.loser.gotravel.ws.response.ResponseListObjects;
import com.loser.gotravel.ws.response.ResponseObjectDetail;

@RestController
@RequestMapping("/api/category")
@Transactional
public class ApiCategoryController extends BaseController {

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public @ResponseBody ResponseObjectDetail<Object> createCategory(
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "description", required = true) String description,
			@RequestParam(value = "image", required = true) String image) {
		Category category = new Category(name, description, image);
		@SuppressWarnings("unchecked")
		List<Category> listCategories = getSession().createCriteria(Category.class).list();
		if (listCategories.size() >= 6) {
			return new ResponseObjectDetail<Object>(true, Message.LIST_CATEGORY_MAXIMUM, null);
		}
		category.createdTime = category.modifiedTime = new Date(System.currentTimeMillis());
		Long autoId = (Long) getSession().save(category);
		if (autoId > 0) {
			return new ResponseObjectDetail<Object>(true, Message.SUCCESS, category);
		} else {
			return new ResponseObjectDetail<Object>(false, Message.CAN_NOT_INSERT_DB, null);
		}
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody ResponseObjectDetail<Object> updateCategry(
			@RequestParam(value = "categoryId", required = true) Long categoryId,
			@RequestParam(value = "name", required = false) String name,
			@RequestParam(value = "description", required = false) String description,
			@RequestParam(value = "image", required = false) String image) {
		Category category = (Category) getSession().get(Category.class, categoryId);
		if (category != null) {
			if (CommonUtils.checkStringValid(name)) category.name = name;
			if (CommonUtils.checkStringValid(description)) category.description = description;
			if (CommonUtils.checkStringValid(image)) category.image = image;
			category.modifiedTime = new Date(System.currentTimeMillis());
			getSession().update(category);
			return new ResponseObjectDetail<Object>(true, Message.SUCCESS, category);
		} 
		return new ResponseObjectDetail<Object>(false, Message.CATEGORY_NOT_FOUND, null);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public @ResponseBody ResponseListObjects<Object> listCategories() {
		List<Category> listCategories = getSession().createCriteria(Category.class).list();
		return new ResponseListObjects<Object>(true, Message.SUCCESS, (Integer) listCategories.size(), listCategories);
	}
	
}
