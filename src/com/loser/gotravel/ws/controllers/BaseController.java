package com.loser.gotravel.ws.controllers;

import javax.servlet.http.HttpSession;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.loser.gotravel.respsec.TokenInfo;

public class BaseController {
	
	protected TokenInfo token;
	
	@Autowired
	protected SessionFactory sessionFactory;
	
	protected Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	protected boolean checkToken(HttpSession httpSession) {
		token = (TokenInfo) getSession().createCriteria(TokenInfo.class).add(Restrictions.eq("token", httpSession.getAttribute("token"))).uniqueResult();
		if (token != null) {
			return true;
		}
		return false;
	}
	
}
