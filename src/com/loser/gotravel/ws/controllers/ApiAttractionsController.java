package com.loser.gotravel.ws.controllers;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.loser.gotravel.dao.AttractionsDao;
import com.loser.gotravel.dao.CommentDao;
import com.loser.gotravel.models.Attractions;
import com.loser.gotravel.models.Comment;
import com.loser.gotravel.models.Rate;
import com.loser.gotravel.models.User;
import com.loser.gotravel.utils.CommonUtils;
import com.loser.gotravel.utils.Message;
import com.loser.gotravel.ws.response.ResponseListObjects;
import com.loser.gotravel.ws.response.ResponseObjectDetail;

@RestController
@RequestMapping("/api/attractions")
@Transactional
public class ApiAttractionsController extends BaseController {

	@Autowired
	AttractionsDao attractionsDao;
	
	@Autowired
	CommentDao commentDao;
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, produces = {"application/json"})
	public @ResponseBody ResponseObjectDetail<Object> create(HttpSession httpSession,
			@RequestBody(required = true) Attractions attractions) {
		if (attractions.categoryId == 0) {
			return new ResponseObjectDetail<Object>(false, Message.ATTRACTIONS_CATEGORYID_NOT_NULL, null);
		} else if (!CommonUtils.checkStringValid(attractions.name)) {
			return new ResponseObjectDetail<Object>(false, Message.ATTRACTIONS_NAME_NOT_NULL, null);
		} if (attractions.informations == null) {
			return new ResponseObjectDetail<Object>(false, Message.ATTRACTIONS_INFORMATION_NOT_NULL, null);
		}
		attractions.createdTime = attractions.modifiedTime = new Date(System.currentTimeMillis());
		Long autoId = attractionsDao.saveItem(attractions);
		if (autoId > 0) {
			return new ResponseObjectDetail<Object>(true, Message.SUCCESS, attractions);
		}
		return new ResponseObjectDetail<Object>(false, Message.CAN_NOT_INSERT_DB, null);
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST, produces = {"application/json"})
	public @ResponseBody ResponseObjectDetail<Object> update(HttpSession httpSession,
			@RequestBody(required = true) Attractions attractionsUpdate) {
		if (attractionsUpdate.attractionsId == 0) {
			return new ResponseObjectDetail<Object>(false, Message.ATTRACTIONS_ID_NOT_NULL, null);
		}
		Attractions attractionsExist = attractionsDao.getActtractionsById(attractionsUpdate.attractionsId);
		if (attractionsExist == null) {
			return new ResponseObjectDetail<Object>(false, Message.ATTRACTIONS_CAN_NOT_FOUND, null);
		}
		
		if (CommonUtils.checkStringValid(attractionsUpdate.address)) attractionsExist.address = attractionsUpdate.address;
		if (CommonUtils.checkStringValid(attractionsUpdate.name)) attractionsExist.name = attractionsUpdate.name;
		if (attractionsUpdate.informations != null) attractionsExist.informations.addAll(attractionsUpdate.informations);
		
		getSession().update(attractionsExist);
		return new ResponseObjectDetail<Object>(true, Message.SUCCESS, attractionsExist);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public @ResponseBody ResponseListObjects<Object> list(HttpSession httpSession,
			@RequestParam(value = "page", required = false, defaultValue = "1") String page,
			@RequestParam(value = "limit", required = false, defaultValue = "50") String limit,
			@RequestParam(value = "categoryId", required = false) Long categoryId) {
		Criteria criteria = getSession().createCriteria(Attractions.class);
		criteria.addOrder(Order.desc("varRate")).addOrder(Order.desc("modifiedTime"));
		criteria.setFirstResult((Integer.parseInt(page)-1)*Integer.parseInt(limit));
		criteria.setMaxResults(Integer.parseInt(limit));
		if (categoryId != null) {
			criteria.add(Restrictions.eq("categoryId", categoryId));
		}
		List<Attractions> listAttractions = criteria.list(); 
		for (int i = 0; i < listAttractions.size(); i++) {
			listAttractions.get(i).totalRate = getSession().createCriteria(Rate.class).add(Restrictions.eq("attractionsId", listAttractions.get(i).attractionsId)).list().size();
		}
		
		return new ResponseListObjects<Object>(true, Message.SUCCESS, listAttractions.size(), listAttractions);
	}
	
	@RequestMapping(value = "/list/{attractionsId}", method = RequestMethod.GET)
	public @ResponseBody ResponseObjectDetail<Object> list(HttpSession httpSession,
			@PathVariable(value = "attractionsId") Long attractionsId) {
		Attractions attractions = attractionsDao.getActtractionsById(attractionsId);
		if (attractions == null) {
			return new ResponseObjectDetail<Object>(false, Message.ATTRACTIONS_CAN_NOT_FOUND, null);
		}
		attractions.totalRate = getSession().createCriteria(Rate.class).add(Restrictions.eq("attractionsId", attractions.attractionsId)).list().size();
		return new ResponseObjectDetail<Object>(true, Message.SUCCESS, attractions);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/rate", method = RequestMethod.POST)
	public @ResponseBody ResponseObjectDetail<Object> rate(HttpSession httpSession,
			@RequestParam(value = "attractionsId", required = true) Long attractionsId,
			@RequestParam(value = "ratePoint", required = true) Double ratePoint) {
		if (checkToken(httpSession)) {
			Attractions attractions = attractionsDao.getActtractionsById(attractionsId);
			if (attractions == null) {
				return new ResponseObjectDetail<Object>(false, Message.ATTRACTIONS_CAN_NOT_FOUND, null);
			}
			if (ratePoint > 5 || ratePoint < 0) {
				return new ResponseObjectDetail<Object>(false, Message.ATTRACTIONS_RATE_FAIL, null);
			}
			Rate rate = (Rate) getSession().createCriteria(Rate.class)
					.add(Restrictions.eq("attractionsId", attractionsId))
					.add(Restrictions.eq("userId", token.userId)).uniqueResult();
			if (rate != null) {
				rate.rate = ratePoint;
				rate.modifiedTime = new Date(System.currentTimeMillis());
				getSession().update(rate);
				Double varRate = (double) 0;
				List<Rate> listRates = getSession().createCriteria(Rate.class).add(Restrictions.eq("attractionsId", attractionsId)).list();
				for (int i = 0; i < listRates.size(); i++) {
					varRate += (Double) listRates.get(i).rate;
				}
				varRate = varRate / listRates.size();
				attractions.varRate = varRate;
				getSession().update(varRate);
				return new ResponseObjectDetail<Object>(true, Message.SUCCESS, null);
			} else {
				Rate newRate = new Rate();
				newRate.rate = ratePoint;
				newRate.userId = token.userId;
				newRate.attractionsId = attractionsId;
				newRate.createdTime = newRate.modifiedTime = new Date(System.currentTimeMillis());
				Long autoId = (Long) getSession().save(newRate);
				if (autoId > 0) {
					Double varRate = (double) 0;
					List<Rate> listRates = getSession().createCriteria(Rate.class).add(Restrictions.eq("attractionsId", attractionsId)).list();
					for (int i = 0; i < listRates.size(); i++) {
						varRate += (Double) listRates.get(i).rate;
					}
					varRate = varRate / listRates.size();
					attractions.varRate = varRate;
					getSession().update(varRate);
					return new ResponseObjectDetail<Object>(true, Message.SUCCESS, null);
				}
				return new ResponseObjectDetail<Object>(false, Message.CAN_NOT_INSERT_DB, null);
				
			}
		}
		return new ResponseObjectDetail<Object>(true, Message.INVALID_TOKEN, null);
	}
	
	@RequestMapping(value = "/comment/post", method = RequestMethod.POST)
	public @ResponseBody ResponseObjectDetail<Object> comment(HttpSession httpSession,
			@RequestParam(value = "userId", required = true) Long userId,
			@RequestParam(value = "attractionsId", required = true) Long attractionsId,
			@RequestParam(value = "content", required = true) String content) {
		if (checkToken(httpSession)) {
			Comment comment = new Comment();
			User user = (User) getSession().get(User.class, userId);
			if (user == null) {
				return new ResponseObjectDetail<Object>(false, Message.USER_NOT_FOUND, null);
			}
			Attractions attractions = (Attractions) getSession().get(Attractions.class, attractionsId);
			if (attractions == null) {
				return new ResponseObjectDetail<Object>(false, Message.ATTRACTIONS_CAN_NOT_FOUND, null);
			}
			comment.userId = userId;
			comment.attractionsId = attractionsId;
			if (content.length() > 10) {
				return new ResponseObjectDetail<Object>(false, Message.ATTRACTIONS_COMMENT_LENGTH, null);
			}
			comment.content = content;
			comment.createdTime = comment.modifiedTime = new Date(System.currentTimeMillis());
			Long autoId = commentDao.saveComment(comment);
			if (autoId > 0) {
				if (CommonUtils.checkStringValid(user.avatar)) {
					comment.avatar = user.avatar;
				}
				comment.username = user.username;
				return new ResponseObjectDetail<Object>(true, Message.SUCCESS, comment);
			}
		}
		return new ResponseObjectDetail<Object>(false, Message.INVALID_TOKEN, null);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/comment/list", method = RequestMethod.GET)
	public @ResponseBody ResponseListObjects<Object> listComment(HttpSession httpSession,
			@RequestParam(value = "attractionsId", required = false) Long attractionsId,
			@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
			@RequestParam(value = "limit", required = false, defaultValue = "50") Integer limit) {
		Criteria criteria = getSession().createCriteria(Comment.class);
		criteria.addOrder(Order.desc("modifiedTime"));
		criteria.setFirstResult((page-1)*limit);
		criteria.setMaxResults(limit);
		if (attractionsId == null) {
			List<Comment> listComments = getSession().createCriteria(Comment.class).list();
			return new ResponseListObjects<Object>(true, Message.SUCCESS, listComments.size(), listComments);
		}
		List<Comment> listComments = getSession().createCriteria(Comment.class).add(Restrictions.eq("attractionsId", attractionsId)).list();
		return new ResponseListObjects<Object>(true, Message.SUCCESS, listComments.size(), listComments);
	}
	
}
