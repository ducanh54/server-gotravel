package com.loser.gotravel.ws.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.loser.gotravel.models.Like;
import com.loser.gotravel.models.RelationLocation;
import com.loser.gotravel.models.TypeLocation;
import com.loser.gotravel.utils.CommonUtils;
import com.loser.gotravel.utils.Message;
import com.loser.gotravel.ws.response.ResponseListObjects;
import com.loser.gotravel.ws.response.ResponseObjectDetail;

@RequestMapping("/api/relation-location")
@Transactional
@RestController
public class ApiRelationLocationController extends BaseController {
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, produces = {"application/json"})
	public @ResponseBody ResponseObjectDetail<Object> create(@RequestBody(required = true) RelationLocation relationLocation) {
		if (!CommonUtils.checkStringValid(relationLocation.name)) {
			return new ResponseObjectDetail<Object>(false, Message.RELATION_LOCATION_NAME_NOT_NULL, null);
		} else if (!CommonUtils.checkStringValid(relationLocation.type)) {
			return new ResponseObjectDetail<Object>(false, Message.RELATION_LOCATION_TYPE_NOT_NULL, null);
		} else if (relationLocation.contacts == null) {
			return new ResponseObjectDetail<Object>(false, Message.RELATION_LOCATION_CONTACT_NOT_NULL, null);
		}
		if (relationLocation.type.equals("1")) {
			relationLocation.type = TypeLocation.HOTEL.toString();
		} else if (relationLocation.type.equals("2")) {
			relationLocation.type = TypeLocation.RESTAURENT.toString();
		} else if (relationLocation.type.equals("3")) {
			relationLocation.type = TypeLocation.SHOP.toString();
		} else if (relationLocation.type.equals("4")) {
			relationLocation.type = TypeLocation.ENTERTAINMENT.toString();
		} else {
			return new ResponseObjectDetail<Object>(false, Message.RELATION_LOCATION_TYPE_WRONG, null);
		}
		relationLocation.createdTime = relationLocation.modifiedTime = new Date(System.currentTimeMillis());
		Long autoId = (Long) getSession().save(relationLocation);
		if (autoId > 0) {
			return new ResponseObjectDetail<Object>(true, Message.SUCCESS, null);
		}
		return new ResponseObjectDetail<Object>(true, Message.CAN_NOT_INSERT_DB, null);
	}
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/{type}/list", method = RequestMethod.GET)
	public @ResponseBody ResponseListObjects<Object> list(@PathVariable(value = "type") String type,
			@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
			@RequestParam(value = "limit", required = false, defaultValue = "100") Integer limit,
			@RequestParam(value = "attractionsId", required = false) Long attractionsId) {
		Criteria criteria = getSession().createCriteria(RelationLocation.class);
		criteria.addOrder(Order.desc("totalLike"));
		criteria.addOrder(Order.asc("totalUnLike"));
		criteria.setFirstResult((page - 1) * limit);
		criteria.setMaxResults(limit);
		List<RelationLocation> relationLocations = new ArrayList<RelationLocation>();
		if (attractionsId == null || attractionsId == 0) {
			relationLocations = criteria.add(Restrictions.eq("type", type)).list();
		} else {
			relationLocations = criteria.add(Restrictions.and(Restrictions.eq("type", type), Restrictions.eq("attractionsId", attractionsId))).list();
		}
		Integer totalItem = getSession().createCriteria(RelationLocation.class).add(Restrictions.and(Restrictions.eq("type", type), Restrictions.eq("attractionsId", attractionsId))).list().size();
		return new ResponseListObjects<Object>(true, Message.SUCCESS, totalItem, relationLocations);
	}
	
	@RequestMapping(value = "/like", method = RequestMethod.POST)
	public @ResponseBody ResponseObjectDetail<Object> like(
			HttpSession httpSession,
			@RequestParam(value = "relationLocationId", required = true) Long relationLocationId,
			@RequestParam(value = "like", required = true) Integer userLike) {
		if (checkToken(httpSession)) {
			if (userLike > 1 || userLike < 0) {
				return new ResponseObjectDetail<Object>(false, Message.RELATION_LOCATION_USERLIKE, null);
			}
			Like like = (Like) getSession().createCriteria(Like.class)
					.add(Restrictions.eq("userId", token.userId))
					.add(Restrictions.eq("relationLocationId", relationLocationId)).uniqueResult();
			RelationLocation relationLocation = (RelationLocation) getSession().get(RelationLocation.class, relationLocationId);
			if (relationLocation == null) {
				return new ResponseObjectDetail<Object>(false, "Relation Location not found", null);
			}
			if (like != null) {
				if (like.like != userLike) {
						like.like = userLike;
						if (userLike == 1) {
							relationLocation.totalLike++;
							relationLocation.totalUnLike--;
						}
						if (userLike == 0) {
							relationLocation.totalLike--;
							relationLocation.totalUnLike++;
						}
						getSession().update(relationLocation);
						like.createdTime = new Date(System.currentTimeMillis());
						getSession().update(like);
						return new ResponseObjectDetail<Object>(true, Message.SUCCESS, null);
				} else {
					return new ResponseObjectDetail<Object>(false, Message.RELATION_LOCATION_USERLIKED, null);
				}
				
			} else {
				Like newLike = new Like();
				newLike.like = userLike;
				newLike.relationLocationId = relationLocationId;
				newLike.userId = token.userId;
				newLike.createdTime = newLike.modifiedTime = new Date(System.currentTimeMillis());
				getSession().save(newLike);
				if (userLike == 1) {
					relationLocation.totalLike++;
				}
				if (userLike == 0) {
					relationLocation.totalUnLike++;
				}
				getSession().update(relationLocation);
				return new ResponseObjectDetail<Object>(true, Message.SUCCESS, null);
			}
		}
		
		return new ResponseObjectDetail<Object>(false, Message.INVALID_TOKEN, null);
	}
	
}
