package com.loser.gotravel.ws.response;

import com.loser.gotravel.models.User;

public class ResponseDataLogin {
	public String token;
	public User member;

	public ResponseDataLogin() {}
	
	public ResponseDataLogin(String token, User member) {
		super();
		this.token = token;
		this.member = member;
	}
	
}
